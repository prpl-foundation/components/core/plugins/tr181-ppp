%define {
    select PPP.Interface {
        /**
         * IPv6 Control Protocol (IPv6CP) client object for this PPP
         * interface [RFC5072]. IPv6CP only applies to IPv6.
         * @version V1.0
         * type object
         */
        %read-only object IPv6CP {
            /**
             * [IPv6Address] The interface identifier for the local end of the PPP link,
             * negotiated using the IPv6CP Interface-Identifier option [Section 4.1/RFC5072].
             * The identifier is represented as the rightmost 64 bits of an IPv6 address
             * (the leftmost 64 bits MUST be zero and MUST be ignored by the recipient).
             * @version V1.0
             * type string(45)
             */
            %read-only string LocalInterfaceIdentifier {
                default "";
                on action validate call check_maximum_length 45;
                on action validate call is_valid_ipv6;
            }

            /**
             * [IPv6Address] The interface identifier for the remote end of the PPP link,
             * negotiated using the IPv6CP Interface-Identifier option [Section 4.1/RFC5072].
             * The identifier is represented as the rightmost 64 bits of an IPv6 address
             * (the leftmost 64 bits MUST be zero and MUST be ignored by the recipient).
             * @version V1.0
             * type string(45)
             */
            %read-only string RemoteInterfaceIdentifier {
                default "";
                on action validate call check_maximum_length 45;
                on action validate call is_valid_ipv6;
            }
        }
    }
}
