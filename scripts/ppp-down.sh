#!/bin/sh
json=''

for s in `env`; do
  # get key name
  key=${s%%=*}
  # get rest of line, remove first character ('=') and " \ characters
  value=${s#$key}
  value=${value:1}
  value=${value//\"/}
  value=${value//'\'/}
  json=$json', "'$key'": "'$value'"'
done

data='"status": "down"'$json
fncdata='{ "mod_namespace": "ppp-ctrl", "mod_name": "mod-ppp-uci", "fnc": "handle-script", "data": { '$data' } }'

ubus call PPP update "$fncdata"
