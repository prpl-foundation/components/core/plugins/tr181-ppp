/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include "service_ppp.h"


#define ME "service-ppp"
#define TERMINATE_TIMEOUT_MS (10)
#define PROCESS_STILL_RUNNING (1)

typedef struct {
    amxp_subproc_t* proc;
    amxp_slot_fn_t termination_callback;
} ppp_proc_t;

static ppp_proc_t pppd = {
    .proc = NULL,
    .termination_callback = NULL,
};

static bool ppp_daemon_service_running(void) {
    return NULL == pppd.proc ? false : amxp_subproc_is_running(pppd.proc);
}

static void slot_proc_stop(const char* const sig_name,
                           const amxc_var_t* const data,
                           UNUSED void* priv) {
    int pid = amxc_var_dyncast(int64_t, amxc_var_get_key(data, "PID", AMXC_VAR_FLAG_DEFAULT));
    SAH_TRACEZ_INFO(ME, "AMX Signal received : %s => process id = %d", sig_name, pid);
}

static const char* get_nic_name(amxc_var_t* args, amxc_string_t* name) {
    const char* nic_name = NULL;
    when_null(args, exit);
    amxc_string_appendf(name, "nic-%s", GETP_CHAR(args, "ifname"));
    nic_name = amxc_string_get(name, 0);
exit:
    return nic_name;

}

bool ppp_service_start(amxc_var_t* args) {
    bool rc = true;
    const char* proto;
    const char* username;
    const char* password;
    amxc_string_t keepalive;
    amxc_llist_t list;
    amxc_string_t* name;
    const char* reason;
    const char* lcp_echo_retry;
    const char* lcp_echo_interval;
    const char* nic_name;

    amxc_llist_init(&list);
    amxc_string_init(&keepalive, 0);
    amxc_string_new(&name, 0);

    amxc_string_appendf(&keepalive, "%s", GETP_CHAR(args, "keepalive"));
    amxc_string_split_word(&keepalive, &list, &reason);
    lcp_echo_retry = amxc_string_get_text_from_llist(&list, 0);
    lcp_echo_interval = amxc_string_get_text_from_llist(&list, 2);

    username = GETP_CHAR(args, "username");
    password = GETP_CHAR(args, "password");
    nic_name = get_nic_name(args, name);

    const char* arguments[] = {"/usr/sbin/pppd", "nodetach", "ipparam",
        "wan", "ifname", "pppoe-wan", "lcp-echo-interval", lcp_echo_interval,
        "lcp-echo-failure", lcp_echo_retry, "+ipv6", "set", "AUTOIPV6=1", "nodefaultroute",
        "usepeerdns", "maxfail", "1", "user", username, "password", password, "ip-up-script",
        "/etc/ppp/ip-up.d/ppp-up.sh", "ipv6-up-script", "/etc/ppp/ip-up.d/ppp-up.sh", "ip-down-script",
        "/etc/ppp/ip-down.d/ppp-down.sh", "ipv6-down-script", "/etc/ppp/ip-down.d/ppp-down.sh",
        "mtu", "1500", "mru", "1500", "plugin", "rp-pppoe.so", nic_name, NULL};

    when_null(args, exit);
    proto = GETP_CHAR(args, "proto");
    when_null(proto, exit);
    when_true(strcmp(proto, "none") == 0, exit);

    when_str_empty(lcp_echo_retry, exit);
    when_str_empty(lcp_echo_interval, exit);

    if(NULL == pppd.proc) {
        amxp_subproc_new(&pppd.proc);
        amxp_slot_connect(amxp_subproc_get_sigmngr(pppd.proc), "stop", NULL, slot_proc_stop, NULL);
    }
    rc = (0 == amxp_subproc_vstart(pppd.proc, (char**) arguments));

exit:
    amxc_string_delete(&name);
    amxc_string_clean(&keepalive);
    amxc_llist_clean(&list, amxc_string_list_it_free);
    return rc;
}

void ppp_service_terminate(void) {
    if(NULL != pppd.proc) {
        amxp_slot_disconnect(amxp_subproc_get_sigmngr(pppd.proc), "stop", pppd.termination_callback);
        pppd.termination_callback = NULL;

        if(ppp_daemon_service_running()) {
            amxp_subproc_kill(pppd.proc, SIGTERM);
            SAH_TRACEZ_INFO(ME, "service terminate, service is running so needs to be shut down");
            if(PROCESS_STILL_RUNNING == amxp_subproc_wait(pppd.proc, TERMINATE_TIMEOUT_MS)) {
                SAH_TRACEZ_ERROR(ME, "service was still running, forcing quit");
                amxp_subproc_kill(pppd.proc, SIGKILL);
            }
        }
        amxp_subproc_delete(&pppd.proc);
    }
}

bool ppp_service_update(amxc_var_t* args) {
    ppp_service_terminate();
    return ppp_service_start(args);
}

