/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "ppp.h"
#include "ppp_soc_utils.h"

#define ME "soc_utils"

static const char* get_ppp_ctrl(amxd_object_t* intf_obj) {
    const amxc_var_t* ppp_ctrl = amxd_object_get_param_value(intf_obj, PPP_CTRL);

    return GET_CHAR(ppp_ctrl, NULL);
}

static void mod_ppp_build_data_struct(ppp_info_t* ppp, amxc_var_t* data) {
    amxc_var_t params;
    amxc_var_t pppoe_params;
    amxd_object_t* pppoe_obj = NULL;
    bool enabled = false;
    bool ipv4 = false;
    bool ipv6 = false;
    amxc_string_t str;
    uint32_t lcp_echo_period = 0;
    uint32_t lcp_echo_retry = 0;
    uint32_t max_mru_size = 0;

    amxc_var_init(&params);
    amxc_var_init(&pppoe_params);
    amxc_string_init(&str, 0);
    amxd_object_get_params(ppp->intf_obj, &params, amxd_dm_access_protected);

    pppoe_obj = amxd_object_findf(ppp->intf_obj, "PPPoE.");
    when_null_trace(pppoe_obj, exit, ERROR, "PPPoE object for interface %s not found", GETP_CHAR(&params, "Alias"));
    amxd_object_get_params(pppoe_obj, &pppoe_params, amxd_dm_access_protected);

    enabled = GET_BOOL(&params, "Enable") && !(ppp->ll_intf_name == NULL);
    ipv4 = GET_BOOL(&params, "IPCPEnable");
    ipv6 = GET_BOOL(&params, "IPv6CPEnable");
    lcp_echo_period = GET_UINT32(&params, "LCPEcho");
    lcp_echo_retry = GET_UINT32(&params, "LCPEchoRetry");
    max_mru_size = GET_UINT32(&params, "MaxMRUSize");
    amxc_string_setf(&str, "%u %u %u", lcp_echo_retry, lcp_echo_period, max_mru_size);

    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, data, "proto", enabled ? "pppoe" : "none");
    amxc_var_add_key(cstring_t, data, "username", enabled ? GET_CHAR(&params, "Username") : "");
    amxc_var_add_key(cstring_t, data, "password", enabled ? GET_CHAR(&params, "Password") : "");
    amxc_var_add_key(cstring_t, data, "ac_name", enabled ? GET_CHAR(&pppoe_params, "ACName") : "");
    amxc_var_add_key(cstring_t, data, "service_name", enabled ? GET_CHAR(&pppoe_params, "ServiceName") : "");
    amxc_var_add_key(cstring_t, data, "padi_interval", enabled ? GET_CHAR(&pppoe_params, "PADIRetryAlgorithm") : "");
    amxc_var_add_key(cstring_t, data, "padr_interval", enabled ? GET_CHAR(&pppoe_params, "PADRRetryAlgorithm") : "");
    amxc_var_add_key(cstring_t, data, "lcp_conf_req_interval", enabled ? GET_CHAR(&params, "LCPConfRetryAlgorithm") : "");
    amxc_var_add_key(cstring_t, data, "lcp_term_interval", enabled ? GET_CHAR(&params, "LCPTermRetryAlgorithm") : "");
    amxc_var_add_key(cstring_t, data, "ipcp_conf_req_interval", enabled ? GET_CHAR(&params, "IPCPConfRetryAlgorithm") : "");
    amxc_var_add_key(cstring_t, data, "ipcp_term_req_interval", enabled ? GET_CHAR(&params, "IPCPTermRetryAlgorithm") : "");
    amxc_var_add_key(cstring_t, data, "auth_failed_interval", enabled ? GET_CHAR(&params, "AuthenticationFailedRetryAlgorithm") : "");
    amxc_var_add_key(cstring_t, data, "auth_interval", GET_CHAR(&params, "AuthenticationRetryAlgorithm"));
    amxc_var_add_key(cstring_t, data, "auth_protocol", GET_CHAR(&params, "RequiredAuthenticationProtocol"));
    amxc_var_add_key(uint32_t, data, "restart_delay", enabled ? GET_UINT32(&params, "RestartDelay") : 0);
    amxc_var_add_key(cstring_t, data, "lcp_echo_restart_delay", enabled ? GET_CHAR(&params, "LCPEchoRandomRestartDelay") : "");
    amxc_var_add_key(cstring_t, data, "name", GET_CHAR(&params, "Name"));
    amxc_var_add_key(cstring_t, data, "ifname", ppp->ll_intf_name);
    amxc_var_add_key(cstring_t, data, "keepalive", enabled ? amxc_string_get(&str, 0) : "");
    amxc_var_add_key(cstring_t, data, "keepalive_adaptive", enabled ? "0" : "");
    amxc_var_add_key(bool, data, "ipv4", ipv4);
    amxc_var_add_key(bool, data, "ipv6", ipv6);
exit:
    amxc_string_clean(&str);
    amxc_var_clean(&pppoe_params);
    amxc_var_clean(&params);
}

int mod_ppp_execute_function(const char* function, ppp_info_t* ppp) {
    amxc_var_t data;
    amxc_var_t ret;
    const char* ppp_ctrl = NULL;
    int rv = -1;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    when_str_empty(function, exit);
    when_null_trace(ppp, exit, ERROR, "ppp info struct is null");
    when_null_trace(ppp->intf_obj, exit, ERROR, "can not execute %s, no object given", function);

    ppp_ctrl = get_ppp_ctrl(ppp->intf_obj);
    when_str_empty_trace(ppp_ctrl, exit, ERROR, "Empty controller, not executing %s", function);
    SAH_TRACEZ_INFO(ME, "%s -> call implementation (controller = '%s')", function, ppp_ctrl);

    mod_ppp_build_data_struct(ppp, &data);
    rv = amxm_execute_function(ppp_ctrl, MOD_PPP_CTRL, function, &data, &ret);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "function %s failed", function);
    }

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&data);
    return rv;
}
