/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ppp.h"
#include "ppp_subscription_utils.h"

#define ME "subscriptions"


static void update_ppp_status(UNUSED ppp_info_t* ppp, UNUSED const char* netdev_state) {
    const char* new_status = NULL;
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null(ppp, exit);
    if(netdev_state == NULL) {
        new_status = "Unknown";
    } else if(strcmp(netdev_state, "notpresent") == 0) {
        new_status = "NotPresent";
    } else if(strcmp(netdev_state, "down") == 0) {
        new_status = "Down";
    } else if(strcmp(netdev_state, "lowerlayerdown") == 0) {
        new_status = "LowerLayerDown";
    } else if(strcmp(netdev_state, "dormant") == 0) {
        new_status = "Dormant";
    } else if(strcmp(netdev_state, "up") == 0) {
        new_status = "Up";
    } else if(strcmp(netdev_state, "Error") == 0) {
        new_status = "Error";
    } else {
        new_status = "Unknown";
    }

    amxd_trans_select_object(&trans, ppp->intf_obj);
    amxd_trans_set_value(cstring_t, &trans, "Status", new_status);
    if(strcmp(new_status, "Up") == 0) {
        amxd_trans_set_value(cstring_t, &trans, "ConnectionStatus", "Connected");
    } else {
        amxd_trans_set_value(cstring_t, &trans, "ConnectionStatus", "Disconnected");
    }
    when_failed_trace(amxd_trans_apply(&trans, ppp_get_dm()), exit, ERROR,
                      "Failed to update datamodel");

exit:
    amxd_trans_clean(&trans);
    return;
}

static void _netdev_link_state_cb(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  void* const priv) {
    ppp_info_t* ppp = NULL;
    const char* new_state = NULL;
    const char* old_state = NULL;
    const char* ppp_name = NULL;

    when_null_trace(priv, exit, ERROR, "private data should contain ppp");
    ppp = (ppp_info_t*) priv;

    old_state = GETP_CHAR(data, "parameters.State.from");
    new_state = GETP_CHAR(data, "parameters.State.to");
    ppp_name = amxc_var_constcast(cstring_t, amxd_object_get_param_value(ppp->intf_obj, "Name"));

    (void) ppp_name;  // use ppp_name when traces are disabled
    (void) old_state; // use old_state when traces are disabled
    SAH_TRACEZ_INFO(ME, "trying to change %s status from %s to %s", ppp_name, old_state, new_state);

    update_ppp_status(ppp, new_state);

exit:
    return;
}

static void netdev_link_get_initial_state(ppp_info_t* ppp, amxb_bus_ctx_t* ctx) {
    int rv = AMXB_ERROR_UNKNOWN;
    amxc_var_t data;
    const char* new_state = NULL;
    const char* ppp_name = NULL;
    amxc_string_t rel_path;

    amxc_string_init(&rel_path, 0);
    amxc_var_init(&data);

    ppp_name = amxc_var_constcast(cstring_t, amxd_object_get_param_value(ppp->intf_obj, "Name"));

    if(ctx != NULL) {
        amxc_string_setf(&rel_path, "NetDev.Link.[%s].State", ppp_name);
        rv = amxb_get(ctx, amxc_string_get(&rel_path, 0), 0, &data, 1);
        if(rv != AMXB_STATUS_OK) {
            SAH_TRACEZ_ERROR(ME, "NetDev link[%s] not found", ppp_name);
        } else {
            new_state = GETP_CHAR(&data, "0.0.State");
        }
    }
    if(new_state == NULL) {
        SAH_TRACEZ_ERROR(ME, "netdev status for %s could not be found", ppp_name);
    }

    update_ppp_status(ppp, new_state);

    amxc_string_clean(&rel_path);
    amxc_var_clean(&data);
}

static void _netdev_new_link_added_cb(UNUSED const char* const sig_name,
                                      UNUSED const amxc_var_t* const data,
                                      void* const priv) {
    ppp_info_t* ppp = NULL;
    const char* ppp_name = NULL;
    amxb_bus_ctx_t* ctx = NULL;

    when_null_trace(priv, exit, ERROR, "private data should contain ppp");
    ppp = (ppp_info_t*) priv;
    ppp_name = amxc_var_constcast(cstring_t, amxd_object_get_param_value(ppp->intf_obj, "Name"));
    when_str_empty_trace(ppp_name, exit, ERROR, "ppp name is empty: no netdev interface name given");

    SAH_TRACEZ_INFO(ME, "%s interface was added to NetDev", ppp_name);

    ctx = amxb_be_who_has("NetDev.Link.");
    when_null_trace(ctx, exit, WARNING, "ctx not known, can not get initial state");
    netdev_link_get_initial_state(ppp, ctx);

exit:
    return;
}

static void _netdev_link_removed_cb(UNUSED const char* const sig_name,
                                    UNUSED const amxc_var_t* const data,
                                    void* const priv) {
    ppp_info_t* ppp = NULL;
    const char* ppp_name = NULL;

    when_null_trace(priv, exit, ERROR, "private data should contain ppp");
    ppp = (ppp_info_t*) priv;
    ppp_name = amxc_var_constcast(cstring_t, amxd_object_get_param_value(ppp->intf_obj, "Name"));
    when_str_empty_trace(ppp_name, exit, ERROR, "ppp name is empty: no netdev interface name given");

    SAH_TRACEZ_INFO(ME, "%s interface was removed from NetDev", ppp_name);

    update_ppp_status(ppp, "notpresent");

exit:
    return;
}

void netdev_link_state_subscription_new(ppp_info_t* ppp) {
    amxb_bus_ctx_t* ctx = NULL;
    amxc_string_t expr;
    const char* ppp_name = NULL;

    amxc_string_init(&expr, 0);

    when_null_trace(ppp, exit, ERROR, "ppp is empty");
    ppp_name = amxc_var_constcast(cstring_t, amxd_object_get_param_value(ppp->intf_obj, "Name"));
    when_str_empty_trace(ppp_name, exit, ERROR, "name is empty: no netdev interface name given");

    ctx = amxb_be_who_has("NetDev.Link.");
    when_null_trace(ctx, exit, WARNING, "ctx not known, no subscription created");

    amxc_string_setf(&expr, "eobject == 'NetDev.Link.[%s].' && "
                     "contains('parameters.State')", ppp_name);
    if(ppp->netdev_status_subscription != NULL) {
        amxb_subscription_delete(&(ppp->netdev_status_subscription));
    }
    amxb_subscription_new(&(ppp->netdev_status_subscription), ctx, "NetDev.Link.",
                          amxc_string_get(&expr, 0), _netdev_link_state_cb, ppp);
    when_null_trace(ppp->netdev_status_subscription, exit, WARNING, "failed to create subscription");
    netdev_link_get_initial_state(ppp, ctx);

exit:
    amxc_string_clean(&expr);
    return;
}

void netdev_link_state_subscription_delete(ppp_info_t* ppp) {
    when_null(ppp, exit);

    if(ppp->netdev_status_subscription != NULL) {
        amxb_subscription_delete(&(ppp->netdev_status_subscription));
    }

exit:
    return;
}


void netdev_link_added_subscription_new(ppp_info_t* ppp) {
    amxb_bus_ctx_t* ctx = NULL;
    amxc_string_t expr;
    const char* ppp_name = NULL;

    amxc_string_init(&expr, 0);

    when_null_trace(ppp, exit, ERROR, "not a valid ppp interface");
    ppp_name = amxc_var_constcast(cstring_t, amxd_object_get_param_value(ppp->intf_obj, "Name"));
    when_str_empty_trace(ppp_name, exit, ERROR, "ppp name is empty: no netdev interface name given");

    ctx = amxb_be_who_has("NetDev.Link.");
    when_null_trace(ctx, exit, WARNING, "ctx not known, no subscription created");

    amxc_string_setf(&expr, "notification == 'dm:instance-added' && path == 'NetDev.Link.' "
                     "&& parameters.Alias == '%s'",
                     ppp_name);
    amxb_subscription_new(&(ppp->netdev_link_added_subscription), ctx, "NetDev.Link.",
                          amxc_string_get(&expr, 0), _netdev_new_link_added_cb, ppp);
    when_null_trace(ppp->netdev_link_added_subscription, exit, WARNING, "failed to create subscription");

exit:
    amxc_string_clean(&expr);
    return;
}

void netdev_link_added_subscription_delete(ppp_info_t* ppp) {
    when_null(ppp, exit);
    amxb_subscription_delete(&(ppp->netdev_link_added_subscription));
exit:
    return;
}

void netdev_link_removed_subscription_new(ppp_info_t* ppp) {
    amxb_bus_ctx_t* ctx = NULL;
    amxc_string_t expr;
    const char* ppp_name = NULL;

    amxc_string_init(&expr, 0);

    when_null_trace(ppp, exit, ERROR, "not a valid ppp interface");
    ppp_name = amxc_var_constcast(cstring_t, amxd_object_get_param_value(ppp->intf_obj, "Name"));
    when_str_empty_trace(ppp_name, exit, ERROR, "ppp name is empty: no netdev interface name given");

    ctx = amxb_be_who_has("NetDev.Link.");
    when_null_trace(ctx, exit, WARNING, "ctx not known, no subscription created");

    amxc_string_setf(&expr, "notification == 'dm:instance-removed' && path == 'NetDev.Link.' "
                     "&& parameters.Alias == '%s'",
                     ppp_name);
    amxb_subscription_new(&(ppp->netdev_link_removed_subscription), ctx, "NetDev.Link.",
                          amxc_string_get(&expr, 0), _netdev_link_removed_cb, ppp);
    when_null_trace(ppp->netdev_link_removed_subscription, exit, WARNING, "failed to create subscription");

exit:
    amxc_string_clean(&expr);
    return;
}

void netdev_link_removed_subscription_delete(ppp_info_t* ppp) {
    when_null(ppp, exit);

    if(ppp->netdev_link_removed_subscription != NULL) {
        amxb_subscription_delete(&(ppp->netdev_link_removed_subscription));
    }

exit:
    return;
}

void netdev_cleanup(ppp_info_t* ppp) {
    netdev_link_added_subscription_delete(ppp);
    netdev_link_state_subscription_delete(ppp);
    netdev_link_removed_subscription_delete(ppp);
}
