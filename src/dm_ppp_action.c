/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "dm_ppp.h"
#include "ppp_utils.h"

#define ME "ppp"

amxd_status_t _check_is_empty_or_in(amxd_object_t* object,
                                    amxd_param_t* param,
                                    amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    amxc_var_t* const retval,
                                    void* priv) {
    amxd_status_t rv = amxd_status_unknown_error;

    rv = amxd_action_param_check_is_in(object, param, reason,
                                       args, retval, priv);

    if(rv != amxd_status_ok) {
        char* input = amxc_var_dyncast(cstring_t, args);
        if((input == NULL) || (*input == 0)) {
            rv = amxd_status_ok;
        }
        free(input);
    }

    return rv;
}

amxd_status_t _validate_delay(UNUSED amxd_object_t* object,
                              UNUSED amxd_param_t* param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              UNUSED amxc_var_t* const retval,
                              UNUSED void* priv) {
    amxd_status_t rc = amxd_status_unknown_error;
    amxc_var_t data;
    uint32_t min_value = 0;
    uint32_t max_value = 0;

    amxc_var_init(&data);

    when_false_status(reason == action_param_validate, exit, rc = amxd_status_function_not_implemented);

    amxc_var_set(csv_string_t, &data, GET_CHAR(args, NULL));
    amxc_var_cast(&data, AMXC_VAR_ID_LIST);

    when_true_status(amxc_llist_size(amxc_var_constcast(amxc_llist_t, &data)) == 1, exit, rc = amxd_status_ok);

    /* Max value should be greater than minimum value */
    min_value = GETI_UINT32(&data, 0);
    max_value = GETI_UINT32(&data, 1);
    when_false_status(min_value < max_value, exit, rc = amxd_status_invalid_value);

    rc = amxd_status_ok;

exit:
    amxc_var_clean(&data);
    return rc;
}

amxd_status_t _check_is_ncp_in(amxd_object_t* object,
                               amxd_param_t* param,
                               amxd_action_t reason,
                               UNUSED const amxc_var_t* const args,
                               amxc_var_t* const retval,
                               void* priv) {
    amxd_status_t rc = amxd_status_unknown_error;
    amxc_var_t new_args;
    amxd_param_t* new_param = NULL;

    if(reason != action_param_validate) {
        return amxd_status_function_not_implemented;
    }
    /*
     * Instead of checking the value found in args (enabled/disabled) we need to verify that
     * the protocol correspondig to the parameter name (IPCPEnable -> IPCP / IPv6CPEnable -> IPv6CP)
     * is part of the list defined in the private data.
     */
    amxc_var_init(&new_args);
    if(strcmp(param->name, "IPCPEnable") == 0) {
        amxc_var_set(cstring_t, &new_args, "IPCP");
    } else if(strcmp(param->name, "IPv6CPEnable") == 0) {
        amxc_var_set(cstring_t, &new_args, "IPv6CP");
    } else {
        rc = amxd_status_invalid_value;
        goto exit;
    }

    /*
     * In the amxd_action_param_check_is_in function the checked parameter is converted to the type of the param->value.
     * At this moment this is a boolean value (enabled/disabled), so we need to change the parameter to be a string.
     */
    new_param = (amxd_param_t*) malloc(sizeof(amxd_param_t));
    memcpy(new_param, param, sizeof(amxd_param_t));
    amxc_var_set(cstring_t, &(new_param->value), param->name);

    rc = amxd_action_param_check_is_in(object, new_param, reason, &new_args, retval, priv);

exit:
    if(new_param != NULL) {
        amxc_var_clean(&(new_param->value));
        free(new_param);
    }
    amxc_var_clean(&new_args);
    return rc;
}

amxd_status_t _ppp_interface_removed(amxd_object_t* object,
                                     UNUSED amxd_param_t* param,
                                     amxd_action_t reason,
                                     UNUSED const amxc_var_t* const args,
                                     UNUSED amxc_var_t* const retval,
                                     UNUSED void* priv) {
    amxd_status_t rv = amxd_status_invalid_action;
    ppp_info_t* ppp = NULL;

    when_false_trace(reason == action_object_destroy, exit, ERROR,
                     "Wrong reason: got %d, expected %d", reason, action_object_destroy);
    rv = amxd_status_invalid_function_argument;
    when_null_trace(object, exit, ERROR, "Object is null");
    when_false_status(object->type == amxd_object_instance, exit, rv = amxd_status_ok);

    ppp = (ppp_info_t*) object->priv;
    when_null_trace(ppp, exit, ERROR, "Private data is null");

    ppp_info_clean(&ppp);

    rv = amxd_status_ok;

exit:
    return rv;
}

amxd_status_t _lastchange_on_read(amxd_object_t* const object,
                                  UNUSED amxd_param_t* const param,
                                  amxd_action_t reason,
                                  UNUSED const amxc_var_t* const args,
                                  amxc_var_t* const retval,
                                  UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    ppp_info_t* ppp = NULL;
    uint32_t since_change = 0;
    uint32_t uptime = 0;

    if(reason != action_param_read) {
        SAH_TRACEZ_NOTICE(ME, "wrong reason, expected action_param_read(%d) got %d", action_param_read, reason);
        rv = amxd_status_invalid_action;
        goto exit;
    }

    when_null_trace(object, skip, ERROR, "object can not be NULL");
    // when a parent object of port gets read,
    // this function will also be called for the template object
    when_null(object->priv, skip);
    ppp = (ppp_info_t*) object->priv;
    uptime = get_system_uptime();
    when_true_trace(uptime < ppp->last_change, skip, ERROR, "UpTime is smaller than last change");
    since_change = uptime - ppp->last_change;
    rv = amxd_status_ok;

skip:
    when_failed_trace(amxc_var_set_uint32_t(retval, since_change),
                      exit, ERROR, "failed to set parameter lastchange");
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}
