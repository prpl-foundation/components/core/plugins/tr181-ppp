include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all
	$(MAKE) -C mod-ppp-uci/src all
	$(MAKE) -C mod-ppp-direct/src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C mod-ppp-uci/src clean
	$(MAKE) -C mod-ppp-direct/src clean
	$(MAKE) -C test clean

install: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ppp-uci.so $(DEST)/usr/lib/amx/ppp/modules/mod-ppp-uci.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ppp-direct.so $(DEST)/usr/lib/amx/ppp/modules/mod-ppp-direct.so
	$(INSTALL) -D -p -m 0644 odl/tr181-ppp-interface-ipcp.odl $(DEST)/etc/amx/$(COMPONENT)/tr181-ppp-interface-ipcp.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-ppp-interface-ipv6cp.odl $(DEST)/etc/amx/$(COMPONENT)/tr181-ppp-interface-ipv6cp.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-ppp-interface-pppoe.odl $(DEST)/etc/amx/$(COMPONENT)/tr181-ppp-interface-pppoe.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-ppp-interface-stats.odl $(DEST)/etc/amx/$(COMPONENT)/tr181-ppp-interface-stats.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-ppp-interface.odl $(DEST)/etc/amx/$(COMPONENT)/tr181-ppp-interface.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-ppp.odl $(DEST)/etc/amx/$(COMPONENT)/tr181-ppp.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-ppp_definition.odl $(DEST)/etc/amx/$(COMPONENT)/tr181-ppp_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-ppp_mapping.odl
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/defaults.d
	$(foreach odl,$(wildcard odl/defaults.d/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/defaults.d/;)
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(DEST)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0660 acl/cwmp/$(COMPONENT).json $(DEST)$(ACLDIR)/cwmp/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/ppp-up.sh $(DEST)/etc/ppp/ip-up.d/ppp-up.sh
	$(INSTALL) -D -p -m 0755 scripts/ppp-down.sh $(DEST)/etc/ppp/ip-down.d/ppp-down.sh

package: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ppp-uci.so $(PKGDIR)/usr/lib/amx/ppp/modules/mod-ppp-uci.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ppp-direct.so $(PKGDIR)/usr/lib/amx/ppp/modules/mod-ppp-direct.so
	$(INSTALL) -D -p -m 0644 odl/tr181-ppp-interface-ipcp.odl $(PKGDIR)/etc/amx/$(COMPONENT)/tr181-ppp-interface-ipcp.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-ppp-interface-ipv6cp.odl $(PKGDIR)/etc/amx/$(COMPONENT)/tr181-ppp-interface-ipv6cp.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-ppp-interface-pppoe.odl $(PKGDIR)/etc/amx/$(COMPONENT)/tr181-ppp-interface-pppoe.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-ppp-interface-stats.odl $(PKGDIR)/etc/amx/$(COMPONENT)/tr181-ppp-interface-stats.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-ppp-interface.odl $(PKGDIR)/etc/amx/$(COMPONENT)/tr181-ppp-interface.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-ppp.odl $(PKGDIR)/etc/amx/$(COMPONENT)/tr181-ppp.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-ppp_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/tr181-ppp_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-ppp_mapping.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/defaults.d
	$(INSTALL) -D -p -m 0644 odl/defaults.d/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d/
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0660 acl/cwmp/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/cwmp/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/ppp-up.sh $(PKGDIR)/etc/ppp/ip-up.d/ppp-up.sh
	$(INSTALL) -D -p -m 0755 scripts/ppp-down.sh $(PKGDIR)/etc/ppp/ip-down.d/ppp-down.sh
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(eval ODLFILES += odl/tr181-ppp.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C mod-ppp-uci/test run
	$(MAKE) -C mod-ppp-uci/test coverage
	$(MAKE) -C mod-ppp-direct/test run
	$(MAKE) -C mod-ppp-direct/test coverage
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test