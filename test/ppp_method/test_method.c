/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_types.h>

#include <amxo/amxo.h>

#include "../common/mock.h"
#include "../common/common_functions.h"

#include "ppp.h"
#include "dm_ppp.h"
#include "test_method.h"

#define PPP_TEST_ODL "../common/mock.odl"
#define PPP_DEFINITION_ODL "../../odl/tr181-ppp_definition.odl"
#define NETDEV_MOCK_ODL "../common/mock_netdev_dm.odl"

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    resolver_add_all_functions(&parser);

    assert_int_equal(amxo_parser_parse_file(&parser, PPP_TEST_ODL, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, PPP_DEFINITION_ODL, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, NETDEV_MOCK_ODL, root_obj), 0);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    // Register data model
    amxb_register(bus_ctx, &dm);

    assert_int_equal(_ppp_main(AMXO_START, &dm, &parser), 0);

    handle_events();
    return 0;
}

int test_teardown(UNUSED void** state) {
    expect_function_call(stop);
    assert_int_equal(_ppp_main(AMXO_STOP, &dm, &parser), 0);

    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    handle_events();
    test_unregister_dummy_be();
    return 0;
}

void test_method_interface_reset(UNUSED void** state) {
    amxd_object_t* intf = amxd_dm_findf(&dm, "PPP.Interface.1.");
    amxc_var_t retval;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    assert_non_null(intf);
    expect_function_call(stop);
    assert_int_equal(amxd_object_invoke_function(intf, "Reset", &args, &retval), 0);
    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_method_interface_update(UNUSED void** state) {
    amxd_object_t* intf = amxd_dm_findf(&dm, "PPP.");
    amxc_var_t retval;
    amxc_var_t args;
    amxc_var_t* data = NULL;

    assert_non_null(intf);

    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "mod_name", "self");
    amxc_var_add_key(cstring_t, &args, "mod_namespace", "mod-ppp");
    amxc_var_add_key(cstring_t, &args, "fnc", "update-dm");
    data = amxc_var_add_key(amxc_htable_t, &args, "data", NULL);

    assert_int_not_equal(amxd_object_invoke_function(intf, "update", &args, &retval), 0);
    handle_events();

    amxc_var_add_key(cstring_t, &args, "mod_name", "self");
    amxc_var_add_key(cstring_t, &args, "mod_namespace", "mod-ppp");
    amxc_var_add_key(cstring_t, &args, "fnc", "update-dm");
    data = amxc_var_add_key(amxc_htable_t, &args, "data", NULL);
    amxc_var_add_key(cstring_t, data, "device", "eth0");
    assert_int_equal(amxd_object_invoke_function(intf, "update", &args, &retval), 0);
    handle_events();

    amxc_var_add_key(cstring_t, &args, "mod_name", "self");
    amxc_var_add_key(cstring_t, &args, "mod_namespace", "mod-ppp");
    amxc_var_add_key(cstring_t, &args, "fnc", "update-dm");
    data = amxc_var_add_key(amxc_htable_t, &args, "data", NULL);
    amxc_var_add_key(cstring_t, data, "device", "eth0");
    amxc_var_add_key(bool, data, "enabled", true);
    assert_int_equal(amxd_object_invoke_function(intf, "update", &args, &retval), 0);
    handle_events();

    amxc_var_add_key(cstring_t, &args, "mod_name", "self");
    amxc_var_add_key(cstring_t, &args, "mod_namespace", "mod-ppp");
    amxc_var_add_key(cstring_t, &args, "fnc", "update-dm");
    data = amxc_var_add_key(amxc_htable_t, &args, "data", NULL);
    amxc_var_add_key(cstring_t, data, "device", "eth0");
    amxc_var_add_key(bool, data, "enabled", true);
    amxc_var_add_key(bool, data, "update_status", true);
    amxc_var_add_key(cstring_t, data, PPP_AUTHENTICATION, "CHAP");
    amxc_var_add_key(cstring_t, data, PPP_ENCRYPTION, "MPPE");
    amxc_var_add_key(cstring_t, data, PPP_STATUS, "Up");
    amxc_var_add_key(cstring_t, data, PPP_CONNECTIONSTATUS, "Connected");
    amxc_var_add_key(cstring_t, data, PPP_PPPOE_SESSION_ID, "42");
    assert_int_equal(amxd_object_invoke_function(intf, "update", &args, &retval), 0);
    handle_events();

    amxc_var_add_key(cstring_t, &args, "mod_name", "self");
    amxc_var_add_key(cstring_t, &args, "mod_namespace", "mod-ppp");
    amxc_var_add_key(cstring_t, &args, "fnc", "update-dm");
    data = amxc_var_add_key(amxc_htable_t, &args, "data", NULL);
    amxc_var_add_key(cstring_t, data, "device", "eth0");
    amxc_var_add_key(bool, data, "enabled", false);
    amxc_var_add_key(bool, data, "update_status", true);
    assert_int_equal(amxd_object_invoke_function(intf, "update", &args, &retval), 0);
    handle_events();

    amxc_var_add_key(cstring_t, &args, "mod_name", "self");
    amxc_var_add_key(cstring_t, &args, "mod_namespace", "mod-ppp");
    amxc_var_add_key(cstring_t, &args, "fnc", "update-dm");
    data = amxc_var_add_key(amxc_htable_t, &args, "data", NULL);
    amxc_var_add_key(cstring_t, data, "device", "eth0");
    amxc_var_add_key(bool, data, "enabled_v6", true);
    amxc_var_add_key(cstring_t, data, PPP_AUTHENTICATION, "CHAP");
    amxc_var_add_key(cstring_t, data, PPP_ENCRYPTION, "MPPE");
    amxc_var_add_key(cstring_t, data, PPP_STATUS, "Up");
    amxc_var_add_key(cstring_t, data, PPP_CONNECTIONSTATUS, "Connected");
    amxc_var_add_key(cstring_t, data, PPP_PPPOE_SESSION_ID, "42");
    assert_int_equal(amxd_object_invoke_function(intf, "update", &args, &retval), 0);
    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_method_interface_random_delay(UNUSED void** state) {
    amxd_action_t reason = action_param_validate;
    amxc_var_t MyVariant;
    amxc_var_init(&MyVariant);

    amxc_var_set(cstring_t, &MyVariant, "0");
    assert_int_equal(amxd_status_ok, _validate_delay(NULL, NULL, reason, &MyVariant, NULL, NULL));

    amxc_var_set(cstring_t, &MyVariant, "0,120,100");
    assert_int_equal(amxd_status_ok, _validate_delay(NULL, NULL, reason, &MyVariant, NULL, NULL));

    amxc_var_set(cstring_t, &MyVariant, "1");
    assert_int_equal(amxd_status_ok, _validate_delay(NULL, NULL, reason, &MyVariant, NULL, NULL));

    amxc_var_set(cstring_t, &MyVariant, "10,100,1");
    assert_int_equal(amxd_status_ok, _validate_delay(NULL, NULL, reason, &MyVariant, NULL, NULL));

    amxc_var_set(cstring_t, &MyVariant, "10,100,10");
    assert_int_equal(amxd_status_ok, _validate_delay(NULL, NULL, reason, &MyVariant, NULL, NULL));

    amxc_var_set(cstring_t, &MyVariant, "10,100,100");
    assert_int_equal(amxd_status_ok, _validate_delay(NULL, NULL, reason, &MyVariant, NULL, NULL));

    amxc_var_set(cstring_t, &MyVariant, "10,100,1000");
    assert_int_equal(amxd_status_ok, _validate_delay(NULL, NULL, reason, &MyVariant, NULL, NULL));

    amxc_var_set(cstring_t, &MyVariant, "0,1,1");
    assert_int_equal(amxd_status_ok, _validate_delay(NULL, NULL, reason, &MyVariant, NULL, NULL));

    amxc_var_clean(&MyVariant);
}

void test_method_interface_event_loop_fds(UNUSED void** state) {
    amxd_object_t* intf = amxd_dm_findf(&dm, "PPP.");
    amxc_var_t retval;
    amxc_var_t args;
    amxc_var_t fd_list;
    amxc_var_t nr_fds;
    amxc_var_t* data = NULL;

    assert_non_null(intf);
    amxc_var_init(&nr_fds);
    amxc_var_set(int32_t, &nr_fds, 0);

    // Case 1: [FAILS] No handler:
    amxc_var_init(&fd_list);
    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxc_var_set_type(&fd_list, AMXC_VAR_ID_LIST);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "mod_name", "self");
    amxc_var_add_key(cstring_t, &args, "mod_namespace", "mod-ppp");
    amxc_var_add_key(cstring_t, &args, "fnc", "event-loop-add-fd");
    data = amxc_var_add_key(amxc_htable_t, &args, "data", NULL);
    amxc_var_add(fd_t, &fd_list, 13);
    amxc_var_add_key(amxc_llist_t, data, "file_descriptors", amxc_var_constcast(amxc_llist_t, &fd_list));

    // Should fail because no handler string is present.
    assert_int_not_equal(amxd_object_invoke_function(intf, "update", &args, &retval), amxd_status_ok);
    handle_events();
    assert_int_equal(count_fds(&nr_fds), 0); // count_fds return 0 on success, the number of file descriptors is stored in output parameter nr_fds
    assert_int_equal(GET_INT32(&nr_fds, NULL), 0);
    amxc_var_clean(&fd_list);
    amxc_var_clean(&args);
    amxc_var_clean(&retval);

    // Case 2: [SUCCEEDS] All data correct
    amxc_var_init(&fd_list);
    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxc_var_set_type(&fd_list, AMXC_VAR_ID_LIST);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "mod_name", "self");
    amxc_var_add_key(cstring_t, &args, "mod_namespace", "mod-ppp");
    amxc_var_add_key(cstring_t, &args, "fnc", "event-loop-add-fd");
    data = amxc_var_add_key(amxc_htable_t, &args, "data", NULL);
    amxc_var_add_key(cstring_t, data, "handler", "handle-discovery");
    amxc_var_add(fd_t, &fd_list, 13);
    amxc_var_add_key(amxc_llist_t, data, "file_descriptors", amxc_var_constcast(amxc_llist_t, &fd_list));

    assert_int_equal(amxd_object_invoke_function(intf, "update", &args, &retval), amxd_status_ok);
    handle_events();
    assert_int_equal(count_fds(&nr_fds), 0);
    assert_int_equal(GET_INT32(&nr_fds, NULL), 1);
    amxc_var_clean(&fd_list);
    amxc_var_clean(&args);
    amxc_var_clean(&retval);

    // Case 3: [SUCCEEDS] All data correct, different descriptors
    amxc_var_init(&fd_list);
    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxc_var_set_type(&fd_list, AMXC_VAR_ID_LIST);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "mod_name", "self");
    amxc_var_add_key(cstring_t, &args, "mod_namespace", "mod-ppp");
    amxc_var_add_key(cstring_t, &args, "fnc", "event-loop-add-fd");
    data = amxc_var_add_key(amxc_htable_t, &args, "data", NULL);
    amxc_var_add_key(cstring_t, data, "handler", "get-input");
    amxc_var_add(fd_t, &fd_list, 14);
    amxc_var_add(fd_t, &fd_list, 15);
    amxc_var_add_key(amxc_llist_t, data, "file_descriptors", amxc_var_constcast(amxc_llist_t, &fd_list));

    assert_int_equal(amxd_object_invoke_function(intf, "update", &args, &retval), amxd_status_ok);
    handle_events();
    assert_int_equal(count_fds(&nr_fds), 0);
    assert_int_equal(GET_INT32(&nr_fds, NULL), 3);
    amxc_var_clean(&fd_list);
    amxc_var_clean(&args);
    amxc_var_clean(&retval);

    // Case 4: [SUCCEEDS] Cleanup descriptors, no descriptors left after call
    cleanup_fds();
    handle_events();
    assert_int_equal(count_fds(&nr_fds), 0);
    assert_int_equal(GET_INT32(&nr_fds, NULL), 0);
    amxc_var_clean(&fd_list);
    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}
