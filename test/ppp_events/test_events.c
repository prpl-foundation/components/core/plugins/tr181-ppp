/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <poll.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "../common/mock.h"
#include "../common/common_functions.h"

#include "ppp.h"
#include "dm_ppp.h"
#include "test_events.h"

#define PPP_TEST_ODL "../common/mock.odl"
#define PPP_DEFINITION_ODL "../../odl/tr181-ppp_definition.odl"
#define NETDEV_MOCK_ODL "../common/mock_netdev_dm.odl"

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;

static void read_sigalrm(int timeout) {
    sigset_t mask;
    int sfd;
    struct signalfd_siginfo fdsi;
    ssize_t s;
    struct pollfd pfd = {0};

    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);

    sigprocmask(SIG_BLOCK, &mask, NULL);

    sfd = signalfd(-1, &mask, 0);
    pfd.fd = sfd;
    pfd.events = POLLIN;

    assert_true(poll(&pfd, 1, timeout) > 0);
    s = read(sfd, &fdsi, sizeof(struct signalfd_siginfo));
    assert_int_equal(s, sizeof(struct signalfd_siginfo));
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    resolver_add_all_functions(&parser);

    assert_int_equal(amxo_parser_parse_file(&parser, PPP_TEST_ODL, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, PPP_DEFINITION_ODL, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, NETDEV_MOCK_ODL, root_obj), 0);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    // Register data model
    amxb_register(bus_ctx, &dm);

    assert_int_equal(_ppp_main(AMXO_START, &dm, &parser), 0);

    handle_events();
    return 0;
}

int test_teardown(UNUSED void** state) {
    expect_function_call(stop);
    expect_function_call(stop);
    assert_int_equal(_ppp_main(AMXO_STOP, &dm, &parser), 0);

    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    handle_events();
    test_unregister_dummy_be();
    return 0;
}

void test_add_interface(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_priv, true);

    amxd_trans_select_pathf(&trans, "PPP.Interface");
    amxd_trans_add_inst(&trans, 0, "test_intf");
    amxd_trans_set_value(cstring_t, &trans, "Controller", "mod-dummy");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.IP.Interface.2");
    amxd_trans_set_value(cstring_t, &trans, "Name", "wan");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();

    amxd_trans_clean(&trans);
}

void test_enable_interface(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* obj = NULL;

    amxd_trans_init(&trans);

    obj = amxd_dm_findf(&dm, "PPP.Interface.test_intf");
    assert_false(amxd_object_get_value(bool, obj, "Enable", NULL));

    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();

    // Enable timer should expire since we are using mod-dummy
    read_sigalrm(5000);
    amxp_timers_calculate();
    amxp_timers_check();

    amxd_trans_clean(&trans);
}

void test_interface_is_enabled(UNUSED void** state) {
    amxd_object_t* obj = NULL;
    amxd_trans_t trans;
    char* status = NULL;
    char* connectionstatus = NULL;

    //first enable the interface
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PPP.Interface.test_intf");
    amxd_trans_set_bool(&trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    obj = amxd_dm_findf(&dm, "PPP.Interface.test_intf");
    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &trans, "ConnectionStatus", "Connected");
    amxd_trans_set_value(cstring_t, &trans, "Status", "Up");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);

    status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(status, "Up");
    free(status);

    //bring netdev down and see if the state now changes
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.1.");
    amxd_trans_set_cstring_t(&trans, "State", "down");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(status, "Down");
    connectionstatus = amxd_object_get_value(cstring_t, obj, "ConnectionStatus", NULL);
    assert_string_equal(connectionstatus, "Disconnected");

    free(status);
    free(connectionstatus);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.1.");
    amxd_trans_set_cstring_t(&trans, "State", "dormant");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(status, "Dormant");
    connectionstatus = amxd_object_get_value(cstring_t, obj, "ConnectionStatus", NULL);
    assert_string_equal(connectionstatus, "Disconnected");

    free(status);
    free(connectionstatus);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.1.");
    amxd_trans_set_cstring_t(&trans, "State", "notpresent");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(status, "NotPresent");
    connectionstatus = amxd_object_get_value(cstring_t, obj, "ConnectionStatus", NULL);
    assert_string_equal(connectionstatus, "Disconnected");

    free(status);
    free(connectionstatus);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.1.");
    amxd_trans_set_cstring_t(&trans, "State", "lowerlayerdown");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(status, "LowerLayerDown");
    connectionstatus = amxd_object_get_value(cstring_t, obj, "ConnectionStatus", NULL);
    assert_string_equal(connectionstatus, "Disconnected");

    free(status);
    free(connectionstatus);

    //bring the interface back up and see if status changes acordingly
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.1.");
    amxd_trans_set_cstring_t(&trans, "State", "up");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(status, "Up");
    connectionstatus = amxd_object_get_value(cstring_t, obj, "ConnectionStatus", NULL);
    assert_string_equal(connectionstatus, "Connected");

    free(status);
    free(connectionstatus);

}

void test_netdev_added_and_delete(UNUSED void** state) {
    amxd_object_t* obj = NULL;
    amxd_object_t* netdev_obj_template = NULL;
    amxd_trans_t trans;
    char* status = NULL;
    char* connectionstatus = NULL;
    amxc_var_t retval;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&retval);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_priv, true);

    amxd_trans_select_pathf(&trans, "PPP.Interface");
    amxd_trans_add_inst(&trans, 0, "test_intf_netdev");
    amxd_trans_set_value(cstring_t, &trans, "Controller", "mod-dummy");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.IP.Interface.2");
    amxd_trans_set_value(cstring_t, &trans, "Name", "test_intf");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    obj = amxd_dm_findf(&dm, "PPP.Interface.test_intf_netdev");

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_priv, true);

    //add the netdev interface
    amxd_trans_select_pathf(&trans, "NetDev.Link");
    amxd_trans_add_inst(&trans, 0, "test_intf");
    amxd_trans_set_value(cstring_t, &trans, "State", "dormant");
    amxd_trans_set_value(cstring_t, &trans, "Name", "test_intf");
    amxd_trans_set_value(uint32_t, &trans, "Index", 2);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(status, "Dormant");
    connectionstatus = amxd_object_get_value(cstring_t, obj, "ConnectionStatus", NULL);
    assert_string_equal(connectionstatus, "Disconnected");

    free(status);
    free(connectionstatus);

    //change the netdev interface state
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.2.");
    amxd_trans_set_cstring_t(&trans, "State", "up");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(status, "Up");
    connectionstatus = amxd_object_get_value(cstring_t, obj, "ConnectionStatus", NULL);
    assert_string_equal(connectionstatus, "Connected");

    free(status);
    free(connectionstatus);

    //delete the netdev interface
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &args, "access", 100);
    amxc_var_add_key(cstring_t, &args, "name", "test_intf");
    netdev_obj_template = amxd_dm_findf(&dm, "NetDev.Link");
    assert_int_equal(amxd_object_invoke_function(netdev_obj_template, "_del", &args, &retval), 0);
    assert_int_equal(amxd_object_get_instance_count(netdev_obj_template), 1);
    handle_events();

    status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(status, "NotPresent");
    connectionstatus = amxd_object_get_value(cstring_t, obj, "ConnectionStatus", NULL);
    assert_string_equal(connectionstatus, "Disconnected");

    free(status);
    free(connectionstatus);

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_disable_interface(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* obj = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    obj = amxd_dm_findf(&dm, "PPP.Interface.test_intf");
    assert_true(amxd_object_get_value(bool, obj, "Enable", NULL));

    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    // Set status up so the disable timer is triggered
    amxd_trans_set_value(cstring_t, &trans, "ConnectionStatus", "Connected");
    amxd_trans_set_value(cstring_t, &trans, "Status", "Up");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();

    // Disable timer should expire since we are using mod-dummy
    read_sigalrm(5000);
    amxp_timers_calculate();
    amxp_timers_check();

    amxd_trans_clean(&trans);
}

void test_remove_interface(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_priv, true);

    amxd_trans_select_pathf(&trans, "PPP.Interface");
    amxd_trans_del_inst(&trans, 0, "test_intf");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();

    amxd_trans_clean(&trans);
}

