# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.15.10 - 2024-11-08(15:01:02 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v0.15.10 - 2024-09-10(07:11:57 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v0.15.9 - 2024-08-01(09:57:53 +0000)

### Other

- - TR-181: Device.PCP data model issues 19.03.2024

## Release v0.15.8 - 2024-07-23(07:57:43 +0000)

### Fixes

- Better shutdown script

## Release v0.15.7 - 2024-07-08(07:09:53 +0000)

## Release v0.15.6 - 2024-05-30(14:55:43 +0000)

## Release v0.15.5 - 2024-05-23(19:49:38 +0000)

### Other

-  [tr181-ppp] Remove uci controller replacement in copy.bara file
- [tr181-ppp] Remove uci controller replacement in copy.bara file

## Release v0.15.4 - 2024-05-22(07:20:32 +0000)

### Fixes

- [tr181-ppp] IPCP (ppp4) is not working

## Release v0.15.3 - 2024-04-10(07:11:28 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.15.2 - 2024-03-29(11:38:48 +0000)

### Fixes

- [tr181-pcm] Saved and defaults odl should not be included in the backup files

## Release v0.15.1 - 2024-02-16(13:55:53 +0000)

### Fixes

- PPP credentials are not Upgade Persistent

## Release v0.15.0 - 2024-02-05(16:20:40 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.14.2 - 2024-01-31(12:51:43 +0000)

### Fixes

- - [tr181-ppp] high cpu usage

## Release v0.14.1 - 2024-01-24(09:21:06 +0000)

### Fixes

- Validate the reason a validation function is called

## Release v0.14.0 - 2024-01-17(09:31:33 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.13.4 - 2023-11-30(09:00:59 +0000)

### Other

- Set lla controller in the component

## Release v0.13.3 - 2023-10-17(15:08:47 +0000)

### Fixes

-  [IPv6 Enable][UserSetting] It must be possible to Enable Ipv6 on the HGW

## Release v0.13.2 - 2023-10-13(14:02:57 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v0.13.1 - 2023-09-19(15:10:57 +0000)

### Fixes

- [tr181-pcm] All the plugins that are register to PCM should set the pcm_svc_config

## Release v0.13.0 - 2023-06-23(09:51:22 +0000)

### New

- Perform a graceful shutdown when a stop signal is received

## Release v0.12.1 - 2023-06-23(08:29:18 +0000)

### Fixes

- Hide Password of the PPP interface

## Release v0.12.0 - 2023-06-07(20:07:06 +0000)

### New

- add ACLs permissions for cwmp user

## Release v0.11.5 - 2023-06-06(10:37:07 +0000)

### Fixes

- [tr181-ppp] DM value MaxMRUSize ignored by plugin and CurrentMRUSize not updated in DM

## Release v0.11.4 - 2023-05-23(15:24:31 +0000)

## Release v0.11.3 - 2023-05-11(09:18:41 +0000)

### Other

- [Coverage] Remove SAHTRACE defines in order to increase branching coverage

## Release v0.11.2 - 2023-04-27(06:20:18 +0000)

### Fixes

- tr181-ppp.sh: fix not starting service

## Release v0.11.1 - 2023-04-25(14:31:08 +0000)

### Fixes

- [tr181-ppp] validate_delay is not working properly

## Release v0.11.0 - 2023-04-24(09:32:28 +0000)

### New

- [amx][prpl][tr181-ppp] Retransmission schemes must be configurable for PPP IPCP/IP6CP/LCP

## Release v0.10.3 - 2023-04-21(10:20:41 +0000)

### Fixes

- Handle a dot in the NetDev name

## Release v0.10.2 - 2023-04-05(16:42:40 +0000)

### Fixes

- tr181-ppp *.LastChanges are wrong in datamodel

## Release v0.10.1 - 2023-03-21(14:04:03 +0000)

### Fixes

- [tr181-dhcpv4][tr181-ppp][tr181-time] unit test code lines are counted in coverage report

## Release v0.10.0 - 2023-03-20(09:50:00 +0000)

### New

- [amx][prpl][tr181-ppp] Retransmission schemes must be configurable for PPPoe PADI/PADR

## Release v0.9.8 - 2023-03-09(09:19:01 +0000)

### Other

- Add missing runtime dependency on rpcd
- [Config] enable configurable coredump generation

## Release v0.9.7 - 2023-02-23(12:37:45 +0000)

### Fixes

- [tr181-ppp] PPPoE parameters in datamodel should be used

## Release v0.9.6 - 2023-02-23(11:40:37 +0000)

### Fixes

- [TR181 PPP] pppoe-wan interface is created if eth0 comes up in demo_wanmode

## Release v0.9.5 - 2023-02-16(11:55:41 +0000)

### Other

- tr181-ppp: documentation generation fails

## Release v0.9.4 - 2023-02-16(11:35:09 +0000)

### Fixes

- [wan-manager] PPP connects twice when behind a VLAN.

## Release v0.9.3 - 2023-02-16(10:22:44 +0000)

### Other

- Add missing STAGING_LIBDIR to LDFLAGS

## Release v0.9.2 - 2023-02-02(15:04:06 +0000)

### Fixes

- [TR181 PPP] PPP blocking during PPPoE Discovery

## Release v0.9.1 - 2023-02-02(11:09:20 +0000)

### Changes

- Use mod-ppp-uci controller as default

## Release v0.9.0 - 2023-01-26(12:14:31 +0000)

### New

- [ppp][ipv6] It must be possible to support pppv6 (ip6cp) with the ppp plugin

## Release v0.8.2 - 2023-01-09(10:15:07 +0000)

### Fixes

- avoidable copies of strings, htables and lists

## Release v0.8.1 - 2022-12-09(09:31:00 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v0.8.0 - 2022-11-16(12:51:27 +0000)

### New

- [TR181-ppp] Create plugin to control pppd

## Release v0.7.4 - 2022-08-24(10:21:15 +0000)

### Fixes

- Buffer ppp script data in case the lowerlayer interface is still unknown

## Release v0.7.3 - 2022-07-29(08:19:15 +0000)

### Fixes

- [tr181-ppp] fix installation path of direct module

## Release v0.7.2 - 2022-07-26(07:51:24 +0000)

### Fixes

- [tr181-ppp] Datamodel in error while the HGW got an IP from the the PPP server

## Release v0.7.1 - 2022-07-20(08:35:25 +0000)

### Changes

- [tr181-ppp] default ppp module should be uci iso direct

## Release v0.7.0 - 2022-07-14(09:07:58 +0000)

### New

- Issue:  HOP-1572 [tr181-ppp] Make module which configures ppp without netifd

## Release v0.6.3 - 2022-07-05(07:05:42 +0000)

### Changes

- [tr181-ppp] Use a netdev subscription to track the status of the ppp interface and update the status

## Release v0.6.2 - 2022-07-04(11:31:21 +0000)

### Other

- Opensource component

## Release v0.6.1 - 2022-06-23(17:17:25 +0000)

### Other

- [amxrt] All amx plugins should start with the -D option

## Release v0.6.0 - 2022-06-17(09:19:42 +0000)

### New

- [PCM] [PPP] [upgrade persistency] Mark PPP Parameters as upgrade persistent

## Release v0.5.1 - 2022-06-09(08:12:22 +0000)

### Fixes

- - [tr181-ppp] Use netmodel queries to find the linux interface

## Release v0.5.0 - 2022-05-23(19:51:01 +0000)

### New

- [netmodel][client] Set default name to match netdevname

## Release v0.4.0 - 2022-05-19(09:15:57 +0000)

### New

- [netmodel][client] Add support for ppp clients in netmodel

## Release v0.3.1 - 2022-05-17(06:09:55 +0000)

### Other

- [TR181-ppp] Unit tests for UCI implementation

## Release v0.3.0 - 2022-05-10(14:16:31 +0000)

### New

- [TR181-ppp] Implement parameters to set the keepalive behavior

## Release v0.2.0 - 2022-05-04(15:56:16 +0000)

### New

- [TR181-ppp] Implementation using UCI

## Release v0.1.2 - 2022-03-24(10:40:25 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v0.1.1 - 2022-02-25(11:42:22 +0000)

### Other

- Enable core dumps by default

## Release v0.1.0 - 2022-02-04(07:22:25 +0000)

### New

- Support of a tr181 compatible ppp plugin

